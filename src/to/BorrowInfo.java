package to;

import java.sql.Timestamp;

public class BorrowInfo {
    private int id;
    private String name;
    private int price;
    private int penalty;
    private Timestamp endDate;

    public BorrowInfo(int id, String name, int price, int penalty, Timestamp endDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.penalty = penalty;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", penalty=" + penalty +
                ", endDate=" + endDate +
                '}';
    }
}
