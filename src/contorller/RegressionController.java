package contorller;

import to.BorrowInfo;
import model.entity.Result;
import model.service.RegressionService;

import java.util.List;
import java.util.Scanner;

public class RegressionController {
    private static RegressionController regressionController =new RegressionController();

    private RegressionController(){}

    public static RegressionController getInstance()
    {
        return regressionController;
    }

    public void chooseRegression(String username)
    {
        try {
            List<BorrowInfo> borrowInfoList = RegressionService.getInstance().getBorrowDtoInfo(username);
            for (BorrowInfo borrowInfo : borrowInfoList
                    ) {
                System.out.println(borrowInfo);
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("Which Id : ");
            int id = scanner.nextInt();

            System.out.println("Today Is : ");
            scanner.nextLine();
            String regressionDate = scanner.nextLine();
            Result result = RegressionService.getInstance().regress(id, username, regressionDate);
            switch (result.getResultStatus()) {
                case OK:
                    int diff = Integer.parseInt(result.getMessage());
                    if (diff > 0) {
                        System.out.println("Dir shode !!");
                    } else if (diff == 0) {
                        System.out.println("sare vaght !!");
                    } else {
                        System.out.println("Thanks; You regressed it");
                    }
                    break;

                case NORESULT:
                    System.out.println("I didnt found it ! Please sure you entered correct data");
                    break;

                case ERROR:
                    System.out.println("Error Happened. Inner Error is : " + result.getMessage());
                    break;

                case EXCEPTION:
                    System.out.println("Sorry; Something wrong happened");
                    break;
            }
        }catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }
}
