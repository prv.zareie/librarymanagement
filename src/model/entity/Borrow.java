package model.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "Borrows")
@Table(name = "BORROWS")

public class Borrow {

    @Id
    @SequenceGenerator(name = "borrowSeq", sequenceName = "BORROW_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "borrowSeq")
    @Column(name = "ID", columnDefinition = "NUMBER")
    private int Id;

    @Column(name = "START_DATE" , columnDefinition = "timestamp")
    private Timestamp startDate;

    @Column(name = "END_DATE" , columnDefinition = "timestamp")
    private Timestamp endDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "BOOK_ID",referencedColumnName="id",nullable=false,unique=true)
    private Book book;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID",referencedColumnName="id",nullable=false,unique=true)
    private User user;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
