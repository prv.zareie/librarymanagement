package model.entity;

import org.terracotta.statistics.Time;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity(name = "Users")
@Table(name = "Users")
public class User {

    @Id
    @SequenceGenerator(name = "userSeq", sequenceName = "USER_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "userSeq")
    @Column(name = "ID", columnDefinition = "NUMBER")
    private int Id;

    @Column(name = "FIRST_NAME" , columnDefinition = "varchar2(20)")
    private String firstName;

    @Column(name = "LAST_NAME" , columnDefinition = "varchar2(20)")
    private String lastName;

    @Column(name = "USERNAME" , columnDefinition = "varchar2(20)")
    private String username;

    @Column(name = "PASSWORD" , columnDefinition = "varchar2(32)")
    private String password;

    @Column(name = "CREATED_AT" , columnDefinition = "timestamp")
    private Timestamp createdAt;

    @Column(name = "NATIONALCODE" , columnDefinition = "char(10)")
    private String nationalcode;

    @Column(name="IS_ENABLED", columnDefinition = "NUMBER(1)")
    private int isEnabled;

    @Column(name="BUDGET", columnDefinition = "NUMBER")
    private int budget;

    /*@OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "USER_ID",nullable = false)
    private Set<Borrow> borrowSet ;*/

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "user")
    private Set<Borrow> borrowSet ;

    public User(String firstName, String lastName, Timestamp createdAt,
                String nationalcode, int isEnabled, int budget,
                String username, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.createdAt = createdAt;
        this.nationalcode = nationalcode;
        this.isEnabled = isEnabled;
        this.budget = budget;
        this.username = username;
        this.password = password;
    }

    public User(String username,String password) {
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public int getId() {
        return Id;
    }

    public User setId(int id) {
        Id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public User setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getNationalcode() {
        return nationalcode;
    }

    public User setNationalcode(String nationalcode) {
        this.nationalcode = nationalcode;
        return this;
    }

    public int getIsEnabled() {
        return isEnabled;
    }

    public User setIsEnabled(int isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    public int getBudget() {
        return budget;
    }

    public User setBudget(int budget) {
        this.budget = budget;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Borrow> getBorrowSet() {
        return borrowSet;
    }

    public void setBorrowSet(Set<Borrow> borrowSet) {
        this.borrowSet = borrowSet;
    }

    public boolean setBorrowPenalty(int penalty)
    {
        if(penalty < 0)
            return false;
        setBudget(getBudget()-penalty);
        return true;
    }
}
