package model.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "Books")
public class Book {

    @Id
    @Column(name = "ID", columnDefinition = "NUMBER")
    @SequenceGenerator(name = "bookseq", sequenceName = "BOOK_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "bookseq")
    private int Id;

    @Column(name="TITLE", columnDefinition = "VARCHAR2(100)")
    private String title;

    @Column(name="SUBJECT", columnDefinition = "VARCHAR2(100)")
    private String subject;

    @Column(name="ISBN", columnDefinition = "VARCHAR2(13)")
    private String isbn;

    @Column(name="IS_BORROWED", columnDefinition = "NUMBER(1)")
    private int isBorrowed;

    @Column(name="PRICE", columnDefinition = "NUMBER")
    private int price;

    @Column(name="PENALTY", columnDefinition = "NUMBER")
    private int penalty;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "book")
    private Set<Borrow> borrowSet ;

    public int getIsBorrowed() {
        return isBorrowed;
    }

    public void setIsBorrowed(int isBorrowed) {
        this.isBorrowed = isBorrowed;
    }

    public Set<Borrow> getBookSet() {
        return borrowSet;
    }

    public void setBookSet(Set<Borrow> borrowSet) {
        this.borrowSet = borrowSet;
    }

    public Book() {
    }

    public Book(String title, String subject, String isbn, int isBorrowed, int price, int penalty) {
        this.title = title;
        this.subject = subject;
        this.isbn = isbn;
        this.isBorrowed = isBorrowed;
        this.price = price;
        this.penalty = penalty;
    }

    public int getId() {
        return Id;
    }

    public Book setId(int id) {
        Id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Book setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public Book setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getIsbn() {
        return isbn;
    }

    public Book setIsbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public boolean isBorrowed() {
        return isBorrowed == 1;
    }

    public Book setBorrowed(int isBorrowed) {
        this.isBorrowed = isBorrowed;
        return this;
    }

    public int getPrice() {
        return price;
    }

    public Book setPrice(int price) {
        this.price = price;
        return this;
    }

    public int getPenalty() {
        return penalty;
    }

    public Book setPenalty(int penalty) {
        this.penalty = penalty;
        return this;
    }

}
