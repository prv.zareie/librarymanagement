package model.entity;

import model.Enum.ResultStatus;

public class Result {
    private ResultStatus resultStatus;

    private String message;

    public Result(ResultStatus resultStatus, String message) {
        this.resultStatus = resultStatus;
        this.message = message;
    }

    public Result() {
        this.resultStatus = resultStatus;
        this.message = message;
    }

    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    public Result setResultStatus(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }
}
