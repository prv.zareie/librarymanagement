package model.service;

import model.entity.Book;
import model.repository.Repository;
import model.repository.RepositoryImp;

import java.util.Map;

public class BookService {
    private static BookService bookService = new BookService();

    public static BookService getInstance() {
        return bookService;
    }

    private BookService(){}

    public boolean isBookBorrowed(int id) throws Exception {
        try {
            Repository<Book> repository = new RepositoryImp();
            Book book = repository.findOne(Book.class, id);
            return book.isBorrowed();
        } catch (Exception ex) {
            throw new Exception();
        }
    }

    public boolean isBookBorrowed(String name) throws Exception {
        try {
            Repository<Book> repository = new RepositoryImp();
            Book book = repository.findWithProperty(Book.class, "TITLE",name);
            return book.isBorrowed();
        } catch (Exception ex) {
            throw new Exception();
        }
    }

    public void borrowBook(int id) throws Exception {
        try {
            Repository<Book> repository = new RepositoryImp();
            Book book = repository.findOne(Book.class, id);
            repository.update(book.setBorrowed(1));
        } catch (Exception ex) {
            throw new Exception();
        }
    }

    public Book getBook(int id) {
        try {
            Repository<Book> repository = new RepositoryImp();
            return repository.findOne(Book.class, id);
        } catch (Exception ex) {
            return null;
        }
    }
}
