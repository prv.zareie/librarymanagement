package model.service;

import model.entity.Book;
import model.entity.Borrow;
import model.entity.User;
import model.repository.Repository;
import model.repository.RepositoryImp;
import model.repository.UserRepository;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;


public class BorrowService {
    private static BorrowService borrowService = new BorrowService();

    public static BorrowService getInstance() {
        return borrowService;
    }

    private BorrowService() {
    }

    public boolean borrow(int id, String username) throws Exception {
        try(Repository repository = new RepositoryImp()){
            if (BookService.getInstance().isBookBorrowed(id))
                throw new Exception("InvalidBorrowRequest");
            else {
                Book book = (Book)repository.findOne(Book.class,id);
                book.setBorrowed(1);
                repository.update(book);

                User user = (User)repository.findWithProperty(User.class,"username",username);

                Borrow borrow = new Borrow();
                borrow.setStartDate(new Timestamp(new Date().getTime()));
                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                c.add(Calendar.DATE, 5);
                Date date = c.getTime();
                borrow.setEndDate(new Timestamp(date.getTime()));
                borrow.setUser(user);
                borrow.setBook(book);
                repository.save(borrow);

                return true;
            }
        }catch  (Exception ex)
        {
            return false;
        }
    }
}
