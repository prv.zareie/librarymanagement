package model.service;

import model.Enum.LoginStatus;
import model.entity.User;
import model.repository.Repository;
import model.repository.UserRepository;
import utility_class.Utility;

import java.util.HashMap;
import java.util.Map;

public class UserService {
    private static UserService userService = new UserService();
    private static Map<String, Integer> usersList;

    public static UserService getInstance() {
        return userService;
    }

    private UserService() {
        usersList = new HashMap<>();
    }

    public LoginStatus login(String username, String Password) {
        try {
            if (canLogin(username) == LoginStatus.OK) {
                Repository<User> repository = new UserRepository();
                User user = repository.findOne(User.class, 1);
                if (user.getPassword().equals(Utility.getMd5(Password)))
                    return LoginStatus.OK;
                else {
                    if (!usersList.containsKey(username)) {
                        usersList.put(username, 0);
                    }

                    int counter = usersList.get(username);
                    usersList.remove(username);
                    usersList.put(username,counter+1);
                    return LoginStatus.WrongPassword;
                }
            }
            else
            {
                return LoginStatus.LockedUser;
            }
        } catch (Exception ex) {
            return LoginStatus.UnknownError;
        }
    }

    public LoginStatus canLogin(String username) {
        if (usersList.containsKey(username)) {
            if (usersList.get(username) == 2) {
                try {
                    lockUser(username);
                } catch (Exception ex) {
                    return LoginStatus.UnknownError;
                }
                return LoginStatus.LockedUser;
            }
        }
        return LoginStatus.OK;
    }

    public User getUser(int id)
    {
        try {
            Repository<User> userRepository = new UserRepository();
            return  userRepository.findOne(User.class,id);
        }catch (Exception ex)
        {
            return null;
        }
    }

    public User getUser(String username)
    {
        try {
            Repository<User> userRepository = new UserRepository();
            return userRepository.findOne(User.class, 1);
        }catch (Exception ex)
        {
            return null;
        }
    }

    public void lockUser(String username) throws Exception
    {
        try(Repository<User> userRepository = new UserRepository())
        {
            userRepository.update(userRepository.findWithProperty(User.class,"username",username));
        }
        catch(Exception ex)
        {
            throw new Exception();
        }
    }
}
