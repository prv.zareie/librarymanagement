package model.service;

import to.BorrowInfo;
import model.Enum.ResultStatus;
import model.entity.Book;
import model.entity.Borrow;
import model.entity.Result;
import model.entity.User;
import model.repository.Repository;
import model.repository.RepositoryImp;
import utility_class.Utility;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

public class RegressionService {
    private static RegressionService regressionService = new RegressionService();
    private RegressionService(){}

    public static RegressionService getInstance(){
        return regressionService;
    }

    public Result controlRegress(int id, String username, String date) throws Exception
    {
        try(Repository repository = new RepositoryImp()){
            Book book = (Book)repository.findOne(Book.class,id);
            Set<Borrow> borrow = book.getBookSet();
            for(Borrow b:borrow)
            {
                System.out.println(b.getUser().getUsername());
                if(b.getUser().getUsername().equals(username)) {
                    if( book.getIsBorrowed() == 0){
                        return new Result().setMessage(String.valueOf("Not Borrowed")).setResultStatus(ResultStatus.ERROR);
                    }
                    Timestamp timestamp = Utility.convertStringToTimestamp(date);
                    int diff = Utility.findDifference(timestamp, b.getEndDate());
                    return new Result().setMessage(String.valueOf(diff)).setResultStatus(ResultStatus.OK);
                }
            }
            return new Result().setResultStatus(ResultStatus.NORESULT);
        }
        catch(Exception ex)
        {
            return new Result().setResultStatus(ResultStatus.EXCEPTION);
        }
    }

    public Result setBookRegression(Book book, User user, boolean hasPenalty,Borrow borrow) throws Exception
    {
        try(Repository repository = new RepositoryImp()){
            book = (Book)repository.findOne(Book.class,book.getId());
            user = (User)repository.findOne(User.class,user.getId());

            book.setIsBorrowed(0);
            if(hasPenalty)
            {
                user.setBorrowPenalty(book.getPenalty());
            }
            repository.update(book);
            repository.update(user);
            repository.delete(borrow);
            return new Result().setResultStatus(ResultStatus.OK);
        }
        catch(Exception ex)
        {
            return new Result().setResultStatus(ResultStatus.EXCEPTION);
        }
    }

    public Result regress(int bookId, String username, String date) throws Exception
    {
        try(Repository repository = new RepositoryImp()){
            Book book = (Book)repository.findOne(Book.class,bookId);
            User user = (User) repository.findWithProperty(User.class,"username",username);
            Set<Borrow> borrowSet = book.getBookSet();
            Borrow borrow =borrowSet.stream().filter(br -> br.getUser().getUsername() == user.getUsername() &&
                    br.getBook().getId() == bookId).reduce((first, second) -> second).get();
            boolean hasPenalty = false;
            Timestamp timestamp = Utility.convertStringToTimestamp(date);
            int diff = Utility.findDifference(borrow.getEndDate(), timestamp);
            if (diff > 0) {
                hasPenalty = true;
            }
            Result result = setBookRegression(book, user, hasPenalty,borrow);
            if(result.getResultStatus() == ResultStatus.OK) {
                return new Result().setMessage(String.valueOf(diff)).setResultStatus(ResultStatus.OK);
            }
            else
            {
                return result;
            }
            //return new Result().setResultStatus(ResultStatus.NORESULT);
        }
        catch(Exception ex)
        {
            return new Result().setResultStatus(ResultStatus.EXCEPTION);
        }
    }

    public List<BorrowInfo> getBorrowDtoInfo(String username){
        try(RepositoryImp<BorrowInfo> repository = new RepositoryImp()) {
            return ((RepositoryImp<BorrowInfo>)repository).getBorrowInfo(username);
        }
        catch(Exception ex)
        {
            return null;
        }
    }
}
