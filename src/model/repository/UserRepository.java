package model.repository;

import model.entity.Borrow;
import model.entity.User;
import utility_class.Utility;

import javax.persistence.Entity;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserRepository implements AutoCloseable,Repository<User>{
    private Connection connection;
    private PreparedStatement preparedStatement;

    public UserRepository() throws Exception
    {
        Class.forName("oracle.jdbc.driver.OracleDriver");
        connection = DriverManager.getConnection("jdbc:oracle:thin:@10.211.55.3:1521:XE", "hr", "hr");
        connection.setAutoCommit(false);
    }

    @Override
    public User findOne(Class<User> aClass, int id) throws Exception {
        User user = new User();
        preparedStatement = connection.prepareStatement("select * from " + aClass.getAnnotation(Entity.class).name() + " where id = ?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            user.setBudget(resultSet.getInt("budget"));
            user.setCreatedAt(resultSet.getTimestamp("created_at"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setIsEnabled(resultSet.getInt("is_enabled"));
            user.setFirstName(resultSet.getString("last_name"));
            user.setNationalcode(resultSet.getString("nationalcode"));
            user.setPassword(resultSet.getString("password"));
            user.setUsername(resultSet.getString("username"));
        }
        return user;
    }

    @Override
    public List<User> findAll(Class<User> aClass) throws Exception {
        List<User> loggingList = new ArrayList();
        preparedStatement = connection.prepareStatement("select * from " + aClass.getAnnotation(Entity.class).name());
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            User user = new User();
            user.setBudget(resultSet.getInt("budget"));
            user.setCreatedAt(resultSet.getTimestamp("created_at"));
            user.setFirstName(resultSet.getString("firstname"));
            user.setIsEnabled(resultSet.getInt("is_enabled"));
            user.setFirstName(resultSet.getString("lastname"));
            user.setNationalcode(resultSet.getString("nationalcode"));
            user.setPassword(resultSet.getString("password"));
            user.setUsername(resultSet.getString("username"));
            loggingList.add(user);

        }
        return loggingList;
    }

    @Override
    public User findWithProperty(Class<User> aClass, String fieldName, String value) throws Exception {
        User user = new User();
        preparedStatement = connection.prepareStatement("select * from " + aClass.getAnnotation(Entity.class).name() +
                " where " + fieldName + " = ?");
        preparedStatement.setString(1, value);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            user.setBudget(resultSet.getInt("budget"));
            user.setCreatedAt(resultSet.getTimestamp("created_at"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setIsEnabled(resultSet.getInt("is_enabled"));
            user.setFirstName(resultSet.getString("last_name"));
            user.setNationalcode(resultSet.getString("nationalcode"));
            user.setPassword(resultSet.getString("password"));
            user.setUsername(resultSet.getString("username"));
        }
        return user;
    }

    @Override
    public void save(User entity) throws Exception {
        preparedStatement = connection.prepareStatement("insert into users values (?,?,?,?,?,?,?,?,?)");
        preparedStatement.setInt(1, 1);
        preparedStatement.setInt(2, entity.getBudget());
        preparedStatement.setTimestamp(3,new Timestamp(new Date().getTime()));
        preparedStatement.setString(4, entity.getFirstName());
        preparedStatement.setInt(5, entity.getIsEnabled());
        preparedStatement.setString(6, entity.getLastName());
        preparedStatement.setString(7, entity.getNationalcode());
        preparedStatement.setString(8, entity.getPassword());
        preparedStatement.setString(9, entity.getUsername());
        preparedStatement.executeUpdate();
    }

    @Override
    public void update(User entity) throws Exception {
        preparedStatement = connection.prepareStatement("update users set is_enabled = 0 where id = ?");
        preparedStatement.setInt(1, entity.getId());
        preparedStatement.executeUpdate();
    }

    @Override
    public void delete(User entity) {
    }

    public String findPassword(String username) throws Exception
    {
        String password = "";
        String u = "";
        preparedStatement = connection.prepareStatement("select password from users where username = ?");
        preparedStatement.setString(1,username);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next())
        {
            password = resultSet.getString("password");
        }
        return password;
    }

    @Override
    public void close() throws Exception {
        connection.commit();
        preparedStatement.close();
    }
}

