package model.repository;


import to.BorrowInfo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

public class RepositoryImp<T> implements Repository<T>  {
    private EntityManagerFactory entityManagerFactory = null;
    private EntityManager entityManager;
    private EntityTransaction entityTransaction;

    public RepositoryImp()
    {
        entityManagerFactory = Persistence.createEntityManagerFactory("MyDB");
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
    }

    public void save(T entity)
    {
        entityManager.persist(entity);
    }

    public void update(T entity) {
        entityManager.merge(entity);
    }

    public void delete(T entity) {
        entity = entityManager.merge(entity);
        entityManager.remove(entity);
    }

    @Override
    public T findWithProperty(Class<T> aClass,String fieldName,String value) {
        Query query = entityManager.createQuery("select entity from " + aClass.getAnnotation(Entity.class).name() + " entity" +
                " where entity." + fieldName + " = '" + value + "'");
        if (!query.getResultList().isEmpty()) {
            return (T) query.getResultList().toArray()[0];
        } else {
            return null;
        }
    }

    @Override
    public T findOne(Class<T> aClass, int id) {
        return entityManager.find(aClass, id);
    }

    @Override
    public List<T> findAll(Class<T> aClass) {
        Query query = entityManager.createQuery("select entity from " + aClass.getAnnotation(Entity.class).name() + " entity");
        return query.getResultList();
    }

    @Override
    public void close() throws Exception {
        entityTransaction.commit();
        entityManager.close();
    }

    public List<BorrowInfo> getBorrowInfo(String username){
        Query query = entityManager.createQuery("select books.id,books.title as name,books.price,books.penalty," +
                "borrows.endDate from Users entity inner join entity.borrowSet borrows" +
                " on entity.id = borrows.user.id inner join borrows.book books on borrows.book.id = books.id" +
                " where entity.username = ? ");
        query.setParameter(0,username);
        List<Object[]> tempList = query.getResultList();
        List<BorrowInfo> borrowInfos = new ArrayList<>();
        for(int i=0;i<tempList.size();i++){
            BorrowInfo borrowInfo = new BorrowInfo((int)tempList.get(i)[0],
                    (String)tempList.get(i)[1],(int)tempList.get(i)[2],(int)tempList.get(i)[3],
                    (Timestamp) tempList.get(i)[4]);
            borrowInfos.add(borrowInfo);
        }
        return borrowInfos;
    }
}
