package model.repository;

import java.util.List;

public interface Repository<T> extends AutoCloseable{
    void save(T entity) throws Exception;

    void update(T entity) throws Exception;

    void delete(T entity) throws Exception;

    T findOne(Class<T> aClass, int id) throws Exception;

    List<T> findAll(Class<T> aClass) throws Exception;

    T findWithProperty(Class<T> aClass,String fieldName,String value) throws Exception;
}
