package model.Enum;

public enum ResultStatus {
    OK,
    NORESULT,
    WARNING,
    ERROR,
    EXCEPTION;
}
