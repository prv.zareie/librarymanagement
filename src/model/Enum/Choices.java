package model.Enum;

public enum Choices {
    BORROWING_BOOK (1),
    REGRESSING_BOOK (2),
    EXIT (3),
    DEFINE_NEW_USER(4),
    UNKNOWN(5);

    private int vlue;

    private Choices(int value)
    {
        this.vlue = value;
    }

    public static Choices getChoice(String choice)
    {
        try {
            Choices c = Choices.values()[Integer.parseInt(choice) - 1];
            return c;
        }
        catch(Exception ex)
        {
            return UNKNOWN;
        }
    }
}
