package model.Enum;

public enum LoginStatus {
    OK,
    WrongPassword,
    InvalidUser,
    LockedUser,
    UnknownError;
}
