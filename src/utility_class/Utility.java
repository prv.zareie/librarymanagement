package utility_class;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Utility {
    public static String getMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static Timestamp convertStringToTimestamp(String date) {


        String line = date + " 00:00:00.000000";
        Timestamp ts = Timestamp.valueOf(line.replace("T"," ").replace("Z",""));

        return ts;
    }


    public static int findDifference(Timestamp timestamp1,Timestamp timestamp2)
    {
        long diff = timestamp2.getTime() - timestamp1.getTime();
        int days = (int) (diff / (24 * 60 * 60 * 1000));
        return days;
    }
}
