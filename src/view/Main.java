package view;

import contorller.BorrowController;
import contorller.RegressionController;
import model.Enum.Choices;
import model.Enum.LoginStatus;
import model.entity.Book;
import model.entity.Result;
import model.repository.Repository;
import model.repository.RepositoryImp;
import model.service.BorrowService;
import model.service.RegressionService;
import model.service.UserService;
import utility_class.Utility;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Timestamp;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {



        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("Enter Username(Enter 3 for exit): ");
                String username = scanner.nextLine().trim();
                if (username.equals("3")) {
                    System.out.println("Bye Bye");
                    return;
                }
                System.out.println("Enter Password(Enter 3 for exit): ");
                String password = scanner.nextLine().trim();
                if (username.equals("3")) {
                    System.out.println("Bye Bye");
                    return;
                }
                LoginStatus loginStatus = UserService.getInstance().login(username, password);
                switch (loginStatus) {
                    case OK: {
                        System.out.println("You logged in;");
                        String choice = "";
                        System.out.println("Please enter 1 for borrowing book; 2 for regressing book; 4 for defining new user; 3 for exit");
                        choice = scanner.nextLine();

                        while (! choice.isEmpty() && choice != "3") {
                            switch (Choices.getChoice(choice)) {
                                case BORROWING_BOOK: {
                                    BorrowController.getInstance().borrow(username);
                                }
                                break;

                                case REGRESSING_BOOK: {
                                    RegressionController.getInstance().chooseRegression(username);
                                }
                                break;

                                case DEFINE_NEW_USER: {
                                    System.out.println("It's not implemented yet :(");
                                }
                                break;

                                case EXIT: {
                                    return;
                                }

                                case UNKNOWN: {
                                    System.out.println("Under construction baby :)");
                                }
                                break;
                            }
                            /*The problem is that nextInt() does not consume the '\n', so the next call to nextLine() consumes it */
                            scanner.nextLine();
                            System.out.println("Please enter 1 for borrowing book; 2 for regressing book; 4 for defining new user; 3 for exit");
                            choice = scanner.nextLine();
                        }
                    }

                    case WrongPassword: {
                        System.out.println("Wrong Password; Try Again;");
                    }
                    break;

                    case LockedUser: {
                        System.out.println("Ooops ! You enetred wrong password more than 2 times and you are locked!");
                        UserService.getInstance().lockUser(username);
                        return;
                    }

                    case UnknownError: {
                        System.out.println("Something is wrong !!");
                        return;
                    }
                }
            }catch (Exception ex){
                String s = ex.getMessage();
            }
        }
    }
}
